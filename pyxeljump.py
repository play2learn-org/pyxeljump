import pyxel
import enum
import time

TILE_GROUND=(4,0)
GRAVITY=1

def blit(tile_x, tile_y, tile_u, tile_v, tilesize=8, colkey=0, img_num=0):
    pyxel.blt(
        x=tile_x,
        y=tile_y,
        img=img_num,
        u=tile_u * tilesize,
        v=tile_v * tilesize,
        w=tilesize,
        h=tilesize,
        colkey=colkey,
    )

def blit_state(tile_x, tile_y, state, direction, anim_num=0, tilesize=8, ignore_color=0, img_num=0):
    blit(
        tile_x=tile_x,
        tile_y=tile_y,
        tile_u=state.value,
        tile_v=direction.value * 2 + anim_num,
        colkey=ignore_color,
    )

class PlayerDirection(enum.Enum):
    RIGHT=0
    LEFT=1

class PlayerState(enum.Enum):
    IDLE=0
    WALK=1
    JUMP=2

def next_state(current_state, player_grounded=True):
    match current_state:
        case PlayerState.IDLE:
            if pyxel.btn(pyxel.KEY_LEFT) or pyxel.btn(pyxel.KEY_RIGHT):
                return PlayerState.WALK
            elif pyxel.btn(pyxel.KEY_SPACE):
                return PlayerState.JUMP
            else:
                return PlayerState.IDLE
        case PlayerState.WALK:
            if pyxel.btn(pyxel.KEY_SPACE):
                return PlayerState.JUMP
            else:
                return PlayerState.WALK
        case PlayerState.JUMP:
            return PlayerState.IDLE if player_grounded else PlayerState.JUMP


class Player:
    def __init__(
        self,
        state=PlayerState.IDLE,
        direction=PlayerDirection.RIGHT,
        start_x=0,
        start_y=0,
    ):
        self.state = state
        self.direction = direction
        self.x = start_x
        self.y = start_y
        self.y_speed = 0

    def as_tile(self, coord):
        return round(coord/8)

    def touching_ground(self):
        raycast_tiles = [pyxel.tilemap(0).pget(self.as_tile(self.x),
                self.as_tile(self.y+i+8)) for i in range(self.y_speed)]
        if not raycast_tiles:
            # at least do the one directly below
            raycast_tiles.append(pyxel.tilemap(0).pget(
                self.as_tile(self.x),
                self.as_tile(self.y+8),
            ))
        print(raycast_tiles)
        return TILE_GROUND in raycast_tiles

    def update(self):
        if self.touching_ground():
            self.y_speed = 0
        elif self.y_speed > 4 or self.y_speed < -GRAVITY*2:
            print("passing on applying speed", self.y_speed)
            pass
        else:
            self.y_speed -= GRAVITY
        self.state = next_state(self.state, self.touching_ground())
        if pyxel.btn(pyxel.KEY_LEFT):
            self.direction = PlayerDirection.LEFT
            self.x-=1
        elif pyxel.btn(pyxel.KEY_RIGHT):
            self.direction = PlayerDirection.RIGHT
            self.x+=1
        elif pyxel.btn(pyxel.KEY_SPACE):
            print("space")
            self.y_speed=4
        self.y-=self.y_speed

    def draw(self, tilesize, ignore_color, img_num):
        blit_state(
            tile_x=self.x,
            tile_y=self.y,
            state=self.state,
            direction=self.direction,
            anim_num=int(pyxel.frame_count % 20 > 10),
            tilesize=tilesize,
            ignore_color=ignore_color,
            img_num=img_num,
        )

class TileMap:
    def __init__(self):
        pass
    def update(self):
        pass
    def draw(self):
        for y in range(20):
            for x in range(20):
                pyxel.bltm(
                    x=x*8,
                    y=y*8,
                    tm=0,
                    u=x*8,
                    v=y*8,
                    w=8,
                    h=8,
                    colkey=0,
                )
class App:
    def __init__(self):
        pyxel.init(160, 120, title="Pyxel Jump")
        self.tilesize = 8
        self.ignore_color = 0
        self.img_num = 0
        pyxel.load("pyxeljump.pyxres")
        self.player = Player(start_x=5*8, start_y=5*8)
        self.tilemap = TileMap()
        pyxel.run(self.update, self.draw)

    def update(self):
        self.player.update()

    def draw(self):
        pyxel.cls(5)
        self.tilemap.draw()
        self.player.draw(self.tilesize, self.ignore_color, self.img_num)

if __name__ == '__main__':
    App()
